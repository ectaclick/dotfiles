#!/bin/bash

if ! command -v mvn >/dev/null 2>&1 
then
    echo "The binary 'mvn' could not be found"
    exit 1
fi

group_name=$1
artefact_id=$2

mvn archetype:generate -DgroupId="$group_name" -DartifactId="$artefact_id" -DarchetypeArtifactId=maven-archetype-quickstart -Dinteractive=false

echo "###> Maven ###
target
###< Maven ###
" >> "./$artefact_id/.gitignore"

echo "You should add exec maven plugin in the ./$2/pom.xml"
echo "
<build>
  <plugins>
    <plugin>
      <groupId>org.codehaus.mojo</groupId>
      <artifactId>exec-maven-plugin</artifactId>
      <version>3.5.0</version>
      <configuration>
        <mainClass>$group_name.App</mainClass>
      </configuration>
    </plugin>
  </plugins>
</build>
"

