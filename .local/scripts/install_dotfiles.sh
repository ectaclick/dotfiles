#!/bin/bash

REPO_URL="https://gitlab.com/ectaclick/dotfiles"

make_backup() {
  if [ -f $1 ]; then
    mv $1 $1.bck && echo "File $1 has been backed up to $1.bck" || echo "Failed to backup file $1"
  fi
}

#git init --bare $HOME/.cfg
#git clone --bare $REPO_URL $HOME/.cfg
#git --git-dir=$HOME/.cfg --work-tree=$HOME config --local status.showUntrackedFiles no

for i in {.zshrc,.bashrc,.commonrc};
do
  make_backup $HOME/$i
done

