#!/bin/bash

edit_prop() {
  local track_identifier=$1
  local prop_identifier=$2
  local new_value=''
  echo "Type the new value"
  read -r new_value
  mkvpropedit "$file" --edit "$track_identifier" --set "$prop_identifier"="$new_value"
}

ensure_command_is_intalled() {
  if ! type "$1" > /dev/null; then
    echo "The command '$1' is not installed"
    exit 1
  else
    echo "Command $1 installed"
  fi
}

ask_for_editing_prop() {
  local prop_name=$1
  local current_value=$2
  local track_identifier=$3
  local prop_identifier=$4

  local edit_prop='n'
  echo "Current value for [$prop_name]:$current_value"
  echo "Do you want to edit property [$prop_name] ? [yN]"
  read -r edit_prop
  if [ "$edit_prop" == 'y' ]; then
    edit_prop "$track_identifier" "$prop_identifier"
  fi
}

########################################
## Main
########################################

ensure_command_is_intalled mkvinfo
ensure_command_is_intalled mkvpropedit

file=$1
printf "Processing file %s\n" $file

info=$(mkvinfo "$file")
echo "Current data"
original_title=$(echo "$info"|grep -i name|head -n 1)

ask_for_editing_prop "Title" "$original_title" "info" "title"

