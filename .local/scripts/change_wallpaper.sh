#!/bin/sh

# Random Wallpaper
CYBERPUNK_WALLPAPER_FOLDER="/data/common/documents/cyberpunk/wallpapers/"
wallpaper_file_path=$CYBERPUNK_WALLPAPER_FOLDER$(ls $CYBERPUNK_WALLPAPER_FOLDER|sort -R|tail -n 1)
ln -sf $wallpaper_file_path $HOME/.wallpaper

