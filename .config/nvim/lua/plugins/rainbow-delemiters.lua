return {
  'HiPhish/rainbow-delimiters.nvim',
  url = 'https://gitlab.com/HiPhish/rainbow-delimiters.nvim',
  config = function(_)
    local config = require('rainbow-delimiters.setup').setup {
      strategy = {
      },
      query = {
      },
      highlight = {
        'RainbowDelimiterRed',
        'RainbowDelimiterYellow',
        'RainbowDelimiterBlue',
        'RainbowDelimiterOrange',
        'RainbowDelimiterGreen',
        'RainbowDelimiterViolet',
        'RainbowDelimiterCyan',
      },
    }
  end,
}
