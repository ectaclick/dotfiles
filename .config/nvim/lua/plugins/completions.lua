return {
  {
    'L3MON4D3/LuaSnip',
    build = 'make install_jsregexp',
    event = 'LspAttach',
    --dependencies = { 'rafamadriz/friendly-snippets' },
    init = function()
      --require('luasnip.loaders.from_vscode').lazy_load()
      require('luasnip.loaders.from_snipmate').lazy_load({
        paths = '~/.config/nvim/snippets/',
      })
    end,
  },
  {
    'hrsh7th/nvim-cmp',
    dependencies = {
      'hrsh7th/cmp-buffer',
      'hrsh7th/cmp-path',
      -- LSP
      'hrsh7th/cmp-nvim-lsp',
      'hrsh7th/cmp-nvim-lsp-document-symbol',
      'hrsh7th/cmp-nvim-lsp-signature-help',
      'saadparwaiz1/cmp_luasnip',
      'onsails/lspkind.nvim',
    },
    event = 'InsertEnter',
    opts = function()
      local cmp = require('cmp')
      local luasnip = require('luasnip')
      local lspkind = require('lspkind')

      require('cmp').setup.cmdline('/', {
        sources = cmp.config.sources({
          { name = 'nvim_lsp_document_symbol' },
        }),
      })

      -- Windows highlight groups
      vim.api.nvim_set_hl(0, 'FloatBorder', {
        fg = '#FF7043',
      })

      return {
        snippet = {
          expand = function(args)
            luasnip.lsp_expand(args.body)
          end,
        },
        sources = cmp.config.sources({
          -- Other sources
          --{ name = 'buffer',                  group_index = 2 },
          { name = 'path' },
          ---- LSP
          { name = 'nvim_lsp', group_index = 1 },
          { name = 'nvim_lsp_document_symbol' },
          { name = 'nvim_lsp_signature_help' },
          ---- Snippet
          { name = 'luasnip' },
        }),
        window = {
          completion = {
            winhighlight = 'Normal:Pmenu,FloatBorder:FloatBorder,Search:None',
            --border = { "╔", "═", "╗", "║", "╝", "═", "╚", "║" }
            border = 'single',
          },
          documentation = {
            winhighlight = 'Normal:Pmenu,FloatBorder:FloatBorder,Search:None',
            --border = { "╔", "═", "╗", "║", "╝", "═", "╚", "║" }
            border = 'single',
          },
        },
        formatting = {
          format = lspkind.cmp_format({
            mode = 'symbol_text',
            maxwidth = {
              -- prevent the popup from showing more than provided characters (e.g 50 will not show more than 50 characters)
              -- can also be a function to dynamically calculate max width such as
              menu = function()
                return math.floor(0.45 * vim.o.columns)
              end,
              --menu = 50,             -- leading text (labelDetails)
              abbr = 50, -- actual suggestion item
            },
            ellipsis_char = '...', -- when popup menu exceed maxwidth, the truncated part would show ellipsis_char instead (must define maxwidth first)
            show_labelDetails = true, -- show labelDetails in menu. Disabled by default
          }),
        },
        mapping = cmp.mapping.preset.insert({
          ['<C-j>'] = cmp.mapping.select_next_item(),
          ['<C-k>'] = cmp.mapping.select_prev_item(),
          ['<C-b>'] = cmp.mapping.scroll_docs(-4),
          ['<C-f>'] = cmp.mapping.scroll_docs(4),
          ['<CR>'] = cmp.mapping.confirm({ select = false }),
          ['<Tab>'] = cmp.mapping(function(fallback)
            if luasnip.expand_or_locally_jumpable() then
              luasnip.expand_or_jump()
            else
              fallback()
            end
          end, { 'i', 's' }),
        }),
      }
    end,
  },
  --config = function()
  --   local cmp = require('cmp')
  --   local luasnip = require('luasnip')

  --   cmp.setup({
  --      snippet = {
  --         expand = function(args)
  --            -- vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
  --            require('luasnip').lsp_expand(args.body)    -- For `luasnip` users.
  --            -- require('snippy').expand_snippet(args.body) -- For `snippy` users.
  --            -- vim.fn["UltiSnips#Anon"](args.body) -- For `ultisnips` users.
  --            -- vim.snippet.expand(args.body) -- For native neovim snippets (Neovim v0.10+)
  --         end,
  --      },
  --      window = {
  --         -- completion = cmp.config.window.bordered(),
  --         -- documentation = cmp.config.window.bordered(),
  --      },
  --      mapping = cmp.mapping.preset.insert({
  --         ["<C-j>"] = cmp.mapping.select_next_item(),
  --         ["<C-k>"] = cmp.mapping.select_prev_item(),
  --         ['<C-b>'] = cmp.mapping.scroll_docs(-4),
  --         ['<C-f>'] = cmp.mapping.scroll_docs(4),
  --         ['<C-Space>'] = cmp.mapping.complete(),
  --         ['<C-e>'] = cmp.mapping.abort(),
  --         ['<CR>'] = cmp.mapping.confirm({ select = false }),    -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
  --         ["<Tab>"] = cmp.mapping(function(fallback)
  --            if luasnip.expand_or_locally_jumpable() then
  --               luasnip.expand_or_jump()
  --            else
  --               fallback()
  --            end
  --         end, { "i", "s" }),
  --         ["<S-Tab>"] = cmp.mapping(function(fallback)
  --            if luasnip.locally_jumpable(-1) then
  --               luasnip.jump(-1)
  --            else
  --               fallback()
  --            end
  --         end, { "i", "s" }),
  --      }),
}
