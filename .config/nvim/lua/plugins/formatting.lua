return {
  'stevearc/conform.nvim',
  event = { 'BufReadPre', 'BufNewFile' },
  opts = {
    formatters_by_ft = {
      c = { 'clang-format' },
      java = { 'clang-format' },
      javascript = { 'prettier' },
      lua = { 'stylua' },
      rust = { 'rustfmt' },
      typescript = { 'prettier' },
      xml = { 'xmlformatter' },
    },
    format_on_save = {
      timeout_ms = 1000,
      lsp_format = 'fallback',
    },
    log_level = vim.log.levels.ERROR,
    -- Conform will notify you when a formatter errors
    notify_on_error = true,
    -- Conform will notify you when no formatters are available for the buffer
    notify_no_formatters = true,
  },
}
