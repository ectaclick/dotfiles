-- Temp fix
-- :Inspect throws an error caused by vim.hl
vim.hl = vim.highlight

--print(#vim.api.nvim_list_bufs())

--function list_buffers_details()
--  local buffers = vim.api.nvim_list_bufs()
--
--  for _, buf in ipairs(buffers) do
--    local name = vim.api.nvim_buf_get_name(buf)  -- Nom du fichier dans le buffer
--    local is_valid = vim.api.nvim_buf_is_valid(buf)  -- Vérifie si le buffer est valide
--    local is_loaded = vim.api.nvim_buf_is_loaded(buf)  -- Vérifie si le buffer est chargé en mémoire
--
--    print(string.format("Buffer ID: %d, Name: %s, Valid: %s, Loaded: %s",
--      buf, name, tostring(is_valid), tostring(is_loaded)))
--  end
--
--  return buffers
--end
--
--function buffers_count()
--    local count = 0;
--
--    for _ = 1, vim.cmd([[buffers]]) do
--       count++
--    end,
--
--    return count
--end
--
--vim.api.nvim_create_buf(true, 'debug')

require('config.options')
require('config.mappings')
require('config.autocmds')
require("config.lazy")

require('colorscheme.global')
